// This is a program to convert non-contiguous labels in a
// segmented volume into contiguous labels.

#include "vtkImageData.h"
#include "vtkNrrdReader.h"
#include "vtkPackLabels.h"
#include "vtkPointData.h"
#include "vtkStructuredPoints.h"
#include "vtkStructuredPointsReader.h"
#include "vtkStructuredPointsWriter.h"
#include "vtkTimerLog.h"
#include "vtkUnsignedIntArray.h"
#include "vtkXMLImageDataReader.h"
#include "vtkXMLImageDataWriter.h"

// Reads data of a few select types. Keys off of input file extension.
vtkSmartPointer<vtkImageData> ReadData(std::string& inFile)
{
  vtkSmartPointer<vtkImageData> output;

  std::string extension = inFile.substr(inFile.find_last_of(".") + 1);

  if (extension == "vti")
  {
    vtkNew<vtkXMLImageDataReader> reader;
    reader->SetFileName(inFile.c_str());
    reader->Update();
    output = reader->GetOutput();
  }
  else if (extension == "vtk")
  {
    vtkNew<vtkStructuredPointsReader> reader;
    reader->SetFileName(inFile.c_str());
    reader->Update();
    output = reader->GetOutput();
  }
  else if (extension == "nrrd")
  {
    vtkNew<vtkNrrdReader> reader;
    reader->SetFileName(inFile.c_str());
    reader->Update();
    output = reader->GetOutput();
  }
  else
  {
    cout << "Unable to open file: " << inFile << "\n";
    exit(1);
  }

  return output;
}

// Write output. Key off of .vtk or .vti extensions. If those aren't
// specified / found, then assume .vti.
void  WriteData(vtkDataSet* output, std::string& outFile)
{
  std::string extension = outFile.substr(outFile.find_last_of(".") + 1);

  // Write diagnostic information
  cout << "Writing data type: " << output->GetPointData()->GetScalars()->GetDataType() << "\n";

  // .vtk creates an ASCII file
  if (extension == "vtk")
  {
    vtkNew<vtkStructuredPointsWriter> spw;
    spw->SetInputData(output);
    spw->SetFileName(outFile.c_str());
    spw->Write();
  }
  else if (extension == "vti")
  {
    vtkNew<vtkXMLImageDataWriter> writer;
    writer->SetInputData(output);
    writer->SetFileName(outFile.c_str());
    writer->Write();
  }
  else
  {
    vtkNew<vtkXMLImageDataWriter> writer;
    writer->SetInputData(output);
    std::string fileName = outFile + ".vti";
    writer->SetFileName(fileName.c_str());
    writer->Write();
  }
}

// Test driver main(). There are up to three command line arguments. The
// first required argument is the name of the labeled volume / annotation
// file. The second (required) argument is the output, packed labeled
// volume. Optionally, a third argument is the type of the output labels
// file. Use the VTK types: VTK_UNSIGNED_CHAR 3, VTK_UNSIGNED_SHORT 5,
// VTK_UNSIGNED_INT 7, or VTK_UNSIGNED_LONG 9. If not specified,
// vtkPackLabels selects the appropriate type.
int main(int argc, char *argv[])
{
  // Command line args. Quick and dirty parsing.
  if ( argc < 3 )
  {
    cout << "Must provide input and output annotation file names\n";
    cout << argv[0] << " inputFileName outputFileName (optional type)\n";
    exit(1);
  }

  // Load in test file
  std::string inFile = argv[1];
  std::string outFile = argv[2];
  vtkSmartPointer<vtkImageData> input = ReadData(inFile);

  // The legacy Multi-material surface nets requires an unsigned
  // short as input. Optionally specify a type
  int specifiedType;
  if ( argc == 4 )
  {
    specifiedType = atoi(argv[3]);
  }
  else
  {
    specifiedType = vtkPackLabels::VTK_DEFAULT_TYPE;
  }

  vtkNew<vtkPackLabels> packer;
  packer->SetInputData(input);
  packer->SetOutputScalarType(specifiedType);
  packer->PassPointDataOff();
  packer->PassCellDataOff();
  packer->PassFieldDataOff();

  vtkNew<vtkTimerLog> timer;
  timer->StartTimer();
  packer->Update();
  timer->StopTimer();

  vtkIdType numLabels = packer->GetNumberOfLabels();
  vtkIdType largestLabel = packer->GetLabels()->GetTuple1(numLabels-1);
  cout << "Largest label: " << largestLabel << "\n";
  cout << "NumLabels: " << numLabels << "\n";
  cout << "Time to pack: " << timer->GetElapsedTime() << "\n";

  // Write the packed output. Assume either ascii .vtk file, or binary .vti file.
  // This will be determined by file extension, if no extension assume .vti.
  WriteData(packer->GetOutput(),outFile);

  return 0;
}
