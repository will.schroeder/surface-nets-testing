// This is a program to run a test suite for various discrete isosurfacing
// algorithms.  The input to the program is a volume with discrete labels
// (e.g., segmented label map). The algorithms executed are:
//
// 1. vtkDiscreteMarchingCubes (MC) - a modification of marching cubes for discrete data
// 2. MMSurfaceNet (MM) - Sarah Frisken's multi-material surface nets
// 3. vtkDiscreteFlyingEdges3D (FE) - a modification of vtkFlyingEdges3D for discrete data
// 4. vtkSurfaceNets3D (PSN) - an parallelized and optimized SurfaceNets algorithm.
//
// Note in an attempt to compare these algorithms fairly, the
// vtkDiscreteMarchingCubes and vtkDiscreteFlyingEdges3D are paired with a
// vtkWindowedSIncPolyDataFilter. This is because MMSurfaceNet and
// vtkSurfaceNets3D algorithms have an internal smoothing pass built into the
// algorithm.
//
// This program runs each algorithm NumSamples times to average wexecution
// times. After each execution, the algorithm memory is released and the
// algorithm is run again until NumSamples executions occur. Also, the
// algorithms MC, MM, FE, and PSN are executed with just one thread to
// compare sequential execution. FE is run with 16 threads, and PSN is run
// for 1, 2, 4, 8, 16 threads.
//
// Note that the input volune is assumed to have N contiguous labels[1,N]
// inclusive, with label[0]==0 the background label. While vtkSurfaceNets3D
// does not require this, other algorithms do, plus is makes it easier to
// test the program.

#include "vtkActor.h"
#include "vtkCellArray.h"
#include "vtkDataArray.h"
#include "vtkDiscreteFlyingEdges3D.h"
#include "vtkDiscreteMarchingCubes.h"
#include "vtkImageData.h"
#include "vtkLookupTable.h"
#include "vtkMath.h"
#include "vtkNrrdReader.h"
#include "vtkPointData.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkSMPTools.h"
#include "vtkStructuredPointsReader.h"
#include "vtkStructuredPoints.h"
#include "vtkSurfaceNets3D.h"
#include "vtkWindowedSincPolyDataFilter.h"
#include "vtkXMLImageDataReader.h"
#include "vtkTimerLog.h"

#include "MMSurfaceNet.h"
#include "MMGeometryOBJ.h"

// Convert data to unsigned short. Yes it could be sped up with templated
// dispatch but it's generally not worth it :-). The reason we do this is
// that the legacy MultiMaterial (MM) surface nets of Frisken assumes that
// the volume is defined with an unsigned short data type.
void ConvertData(vtkDataArray* inScalars, unsigned short* usArray)
{
  vtkIdType numScalars = inScalars->GetNumberOfTuples();
  for ( vtkIdType i=0; i < numScalars; ++i )
  {
    usArray[i] = static_cast<unsigned short>(inScalars->GetTuple1(i));
  }
}

// Define test parameters
struct TestParams
{
  std::string                     FileName;
  vtkSmartPointer<vtkImageData>   Image;
  unsigned short*                 Data;
  int                             Dims[3];
  double                          Spacing[3];
  int                             NumLabels;
  int                             NumSmoothingIters;
  vtkSmartPointer<vtkLookupTable> LUT;
  bool                            Display;
  int                             NumThreads;
  int                             NumSamples;

  // Generate a lookup table. Here we are presuming that label 0 is a
  // background label, and that the labels are contiguous ranging from
  // [1,NumLabels].
  void GenerateLUT()
  {
    vtkNew<vtkMath> math;
    this->LUT = vtkSmartPointer<vtkLookupTable>::New();
    vtkLookupTable* lut = this->LUT.Get();
    lut->SetNumberOfColors(this->NumLabels+1);
    lut->SetTableRange(0, this->NumLabels);
    lut->SetScaleToLinear();
    lut->Build();
    lut->SetTableValue(0, 0, 0, 0, 1);
    math->RandomSeed(5071);
    for ( int i=1; i < this->NumLabels; ++i)
    {
      lut->SetTableValue(i, math->Random(.2, 1),
                            math->Random(.2, 1),
                            math->Random(.2, 1), 1);
    }
  }
};

// Optional display method
void DisplayOutput(vtkPolyData* output, TestParams& testParams)
{
  vtkNew<vtkPolyDataMapper> pdm;
  pdm->SetInputData(output);
  pdm->SetLookupTable(testParams.LUT);
  pdm->SetScalarRange(0, testParams.LUT->GetNumberOfColors());

  vtkNew<vtkActor> actor;
  actor->SetMapper(pdm);

  vtkNew<vtkRenderer> ren;
  ren->AddActor(actor);

  vtkNew<vtkRenderWindow> renWin;
  renWin->SetSize(500,500);
  renWin->AddRenderer(ren);

  vtkNew<vtkRenderWindowInteractor> iren;
  iren->SetRenderWindow(renWin);

  renWin->Render();
  iren->Start();
}

// Execute marching cubes. Return time to average execution results.
double MarchingCubes(TestParams &testParams)
{
  cout << "\nExecuting Marching Cubes\n";
  cout << "\twith " << testParams.NumThreads << " threads\n";
  vtkSMPTools::Initialize(testParams.NumThreads);

  // Time surface nets
  vtkNew<vtkTimerLog> timer;
  double aveTime = 0.0;

  vtkNew<vtkDiscreteMarchingCubes> mc;
  mc->SetInputData(testParams.Image);
  mc->GenerateValues(testParams.NumLabels,1,testParams.NumLabels);

  vtkNew<vtkWindowedSincPolyDataFilter> smooth;
  smooth->SetInputConnection(mc->GetOutputPort());
  smooth->SetNumberOfIterations(testParams.NumSmoothingIters);

  for (int testNum=0; testNum < testParams.NumSamples; ++testNum)
  {
    mc->Modified();
    timer->StartTimer();
    smooth->Update();
    timer->StopTimer();
    aveTime += timer->GetElapsedTime();
  }
  aveTime /= static_cast<double>(testParams.NumSamples);

  timer->StopTimer();
  auto time = timer->GetElapsedTime();
  cout << "Time for Marching Cubes: " << aveTime << "\n";
  cout << "# triangles: " << smooth->GetOutput()->GetNumberOfCells() << "\n";

  if (testParams.Display)
    DisplayOutput(smooth->GetOutput(),testParams);

  return aveTime;
}

// Execute flying edges.  Return time to average execution results.
double FlyingEdges(TestParams& testParams)
{
  cout << "\nExecuting Flying Edges\n";
  cout << "\twith " << testParams.NumThreads << " threads\n";
  vtkSMPTools::Initialize(testParams.NumThreads);

  // Time surface nets
  vtkNew<vtkTimerLog> timer;
  double aveTime = 0.0;

  vtkNew<vtkDiscreteFlyingEdges3D> fe;
  fe->SetInputData(testParams.Image);
  fe->GenerateValues(testParams.NumLabels,1,testParams.NumLabels);

  vtkNew<vtkWindowedSincPolyDataFilter> smooth;
  smooth->SetInputConnection(fe->GetOutputPort());
  smooth->SetNumberOfIterations(testParams.NumSmoothingIters);

  for (int testNum=0; testNum < testParams.NumSamples; ++testNum)
  {
    fe->Modified();
    timer->StartTimer();
    smooth->Update();
    timer->StopTimer();
    aveTime += timer->GetElapsedTime();
  }
  aveTime /= static_cast<double>(testParams.NumSamples);

  cout << "Time for Flying Edges: " << aveTime << "\n";
  cout << "# triangles: " << smooth->GetOutput()->GetNumberOfCells() << "\n";

  if (testParams.Display)
    DisplayOutput(smooth->GetOutput(),testParams);

  return aveTime;
}

// Execute legacy surface nets.  Return time to average execution results.
double LegacyNets(TestParams& testParams)
{
  cout << "\nExecuting Legacy Surface Nets\n";
  cout << "\twith " << testParams.NumThreads << " threads\n";
  vtkSMPTools::Initialize(testParams.NumThreads);

  // Time surface nets
  vtkNew<vtkTimerLog> timer;
  double aveTime = 0.0;

  float voxelSize[3];
  voxelSize[0]=testParams.Spacing[0];
  voxelSize[1]=testParams.Spacing[1];
  voxelSize[2]=testParams.Spacing[2];

  MMSurfaceNet::RelaxAttrs relax;
  relax.numRelaxIterations = testParams.NumSmoothingIters;
  relax.relaxFactor = 0.5;
  relax.maxDistFromCellCenter = 1;

  MMSurfaceNet *SN = nullptr;
  for (int testNum=0; testNum < testParams.NumSamples; ++testNum)
  {
    timer->StartTimer();
    // Surface extraction
    SN = new MMSurfaceNet(testParams.Data,testParams.Dims,voxelSize);
    // Smoothing
    SN->relax(relax);
    // Save the last SN as it may be used for Display
    if ( !testParams.Display || testNum < (testParams.NumSamples-1) ) delete SN;
    timer->StopTimer();
    aveTime += timer->GetElapsedTime();
  }
  aveTime /= static_cast<double>(testParams.NumSamples);

  cout << "Time to extract legacy surface: " << aveTime << "\n";

  // Create output polydata if display desired. Query the surface
  // net for appropriate information.
  if (testParams.Display)
  {
    MMGeometryOBJ geom(SN);
    std::vector<int> labels = geom.labels();
    int numLabels = labels.size();
    cout << "NumLabels: " << numLabels << "\n";

    vtkNew<vtkPolyData> output;
    vtkNew<vtkPoints> pts;
    pts->SetDataTypeToFloat();
    vtkNew<vtkCellArray> tris;
    output->SetPoints(pts);
    output->SetPolys(tris);

    // For each label, add to polydata. Note that the assumption is
    // that label 0 is a background label.
    vtkIdType ptOffset = 0;
    for ( auto l=1; l < numLabels; ++l)
    {
      cout << "\tLabel[" << l << "]: " << labels[l] << "\n";
      MMGeometryOBJ::OBJData obj = geom.objData(labels[l]);
      vtkIdType numPts = obj.vertexPositions.size();
      vtkIdType numTris = obj.triangles.size();

      // Add points.
      for ( auto id = 0; id < numPts; ++id)
      {
        std::array<float,3> &pt = obj.vertexPositions[id];
        pts->InsertNextPoint(pt.data());
      }

      // Add triangles. Deal with point idx offset.
      vtkIdType triIds[3];
      for ( auto id = 0; id < numTris; ++id)
      {
        std::array<int,3> &tri = obj.triangles[id];
        triIds[0] = tri[0] - 1 + ptOffset;
        triIds[1] = tri[1] - 1 + ptOffset;
        triIds[2] = tri[2] - 1 + ptOffset;
        tris->InsertNextCell(3,triIds);
      }
      ptOffset += numPts;
    } //for all non-background labels

    cout << "NumPts,NumTris: (" << pts->GetNumberOfPoints()
         << "," << tris->GetNumberOfCells() << ")\n";
    DisplayOutput(output,testParams);
    delete SN;
  }

  return aveTime;
}

// Execute parallel surface nets. Return time to average execution results.
double SurfaceNets(TestParams& testParams)
{
  cout << "\nExecuting Parallel Surface Nets\n";
  cout << "\twith " << testParams.NumThreads << " threads\n";
  vtkSMPTools::Initialize(testParams.NumThreads);

  // Time surface nets
  vtkNew<vtkTimerLog> timer;
  double aveTime = 0.0;

  vtkNew<vtkSurfaceNets3D> sn;
  sn->SetInputData(testParams.Image);
  sn->GenerateValues(testParams.NumLabels,1,testParams.NumLabels);
  sn->SetNumberOfIterations(testParams.NumSmoothingIters);

  for (int testNum=0; testNum < testParams.NumSamples; ++testNum)
  {
    sn->Modified();
    timer->StartTimer();
    sn->Update();
    timer->StopTimer();
    aveTime += timer->GetElapsedTime();
  }
  aveTime /= static_cast<double>(testParams.NumSamples);

  cout << "Time for Parallel Surface Nets: " << aveTime << "\n";
  cout << "# triangles: " << sn->GetOutput()->GetNumberOfCells() << "\n";

  if (testParams.Display)
    DisplayOutput(sn->GetOutput(),testParams);

  return aveTime;
}

// Reads data of a few select types. Keys off of input file extension.
vtkSmartPointer<vtkImageData> ReadData(TestParams& testParams)
{
  vtkSmartPointer<vtkImageData> output;

  std::string extension = testParams.FileName.substr(testParams.FileName.find_last_of(".") + 1);

  if (extension == "vti")
  {
    vtkNew<vtkXMLImageDataReader> reader;
    reader->SetFileName(testParams.FileName.c_str());
    reader->Update();
    output = reader->GetOutput();
  }
  else if (extension == "vtk")
  {
    vtkNew<vtkStructuredPointsReader> reader;
    reader->SetFileName(testParams.FileName.c_str());
    reader->Update();
    output = reader->GetOutput();
  }
  else if (extension == "nrrd")
  {
    vtkNew<vtkNrrdReader> reader;
    reader->SetFileName(testParams.FileName.c_str());
    reader->Update();
    output = reader->GetOutput();
  }
  else
  {
    cout << "Unable to open file: " << testParams.FileName << "\n";
    exit(1);
  }

  return output;
}

// Test driver main(). There are up to four command line arguments. The first
// required argument is the name of the labeled volume / annotation file. The
// second (required) argument is the number of labels. The third (optional)
// argument is the number of samples/repeated executions to average
// performance (by default NumSamples==1). The fourth (optional) argument is
// a flag indicating whether to display output afer each algorithm execution.
int main(int argc, char *argv[])
{
  // Command line args. Quick and dirty parsing.
  if ( argc < 3 )
  {
    cout << "Must provide annotation file name, required numSamples, optional numExecs, optional display flag\n";
    cout << argv[0] << " fileName numLabels numSamples display\n";
    exit(1);
  }

  // Specify parameters
  TestParams testParams;
  testParams.FileName = argv[1];
  testParams.NumLabels = atoi(argv[2]);
  testParams.NumSamples = ( argc < 4 ? 1 : atoi(argv[3]));
  testParams.Display = (argc < 5 ? false : true);
  cout << "Processing: " << testParams.FileName << " with "
       << testParams.NumLabels << " labels, "
       << testParams.NumSamples << " samples and display "
       << (testParams.Display ? "On" : "Off") << "\n";

  // Load in test file
  vtkSmartPointer<vtkImageData> output = ReadData(testParams);

  // Check and convert data as necessary
  int stype = output->GetScalarType();
  vtkSmartPointer<vtkImageData> imageData;
  if ( stype != VTK_UNSIGNED_SHORT )
  {
    cout << "\nConverting data to ushort\n\n";
    vtkDataArray* inScalars = output->GetPointData()->GetScalars();
    imageData = vtkSmartPointer<vtkImageData>::New();
    imageData->SetDimensions(output->GetDimensions());
    imageData->SetSpacing(output->GetSpacing());
    imageData->AllocateScalars(VTK_UNSIGNED_SHORT,1);
    ConvertData(inScalars, (unsigned short*)imageData->GetScalarPointer());
    testParams.Image = imageData;
  }
  else // no conversion needed
  {
    testParams.Image = output;
  }

  // Get the data
  testParams.Data = (unsigned short*) testParams.Image->GetScalarPointer();
  testParams.Image->GetDimensions(testParams.Dims);
  testParams.Image->GetSpacing(testParams.Spacing);
  cout << "Read volume from: " << testParams.FileName << " with \n\tDimensions: ("
       << testParams.Dims[0] << "," << testParams.Dims[1] << "," << testParams.Dims[2] << ")\n";
  cout << "\tSpacing: ("
       << testParams.Spacing[0] << "," << testParams.Spacing[1] << "," << testParams.Spacing[2] << ")\n";

  // Set remaining control parameters
  testParams.NumSmoothingIters = 25;
  cout << "\tNumLabels, NumSmoothingIters: (" << testParams.NumLabels << ", " << testParams.NumSmoothingIters << ")\n";
  testParams.GenerateLUT(); // Lookup table for displaying data

  // Execute different algorithms. The vtkSMPTools::Initialize(numThreads)
  // is used to control threading. In some cases we want to run sequential
  // by setting numThreads=1. This affects not only the surface extraction,
  // but also the smoothing operation.

  // Keep track of averaged execution times.
  double execTimes[9] = {1.0};

  // Marching Cubes. Discrete marching cubes is always sequential, but
  // by increasing the number of threads >1 the WindowedSinc smoothing
  // filter runs faster.
  testParams.NumThreads = 1;
  execTimes[0] = MarchingCubes(testParams);

  // Flying edges, test in serial and with maximum number of threads. Both
  // surface extraction and smoothing are affected by the thread count.
  testParams.NumThreads = 1;
  execTimes[1] = FlyingEdges(testParams);
  testParams.NumThreads = 16;
  execTimes[2] = FlyingEdges(testParams);

  // Legacy surface nets. Always runs sequential; changing thread count
  // makes no difference.
  testParams.NumThreads = 1;
  execTimes[3] = LegacyNets(testParams);

  // Parallel surface nets. Both surface extraction and constrained smoothing
  // are affected by the thread count.
  testParams.NumThreads = 1;
  execTimes[4] = SurfaceNets(testParams);
  testParams.NumThreads = 2;
  execTimes[5] = SurfaceNets(testParams);
  testParams.NumThreads = 4;
  execTimes[6] = SurfaceNets(testParams);
  testParams.NumThreads = 8;
  execTimes[7] = SurfaceNets(testParams);
  testParams.NumThreads = 16;
  execTimes[8] = SurfaceNets(testParams);

  // Output test results
  double sup[9];
  for (auto i=0; i<9; ++i)
  {
    sup[i] = execTimes[0] / execTimes[i];
  }
  cout << "MC:" << "\t" << execTimes[0] << "secs, \tSpeedUp: " << sup[0] << "\n";
  cout << "FE(1):" << "\t" << execTimes[1] << "secs, \tSpeedUp: " << sup[1] << "\n";
  cout << "FE(16):" << "\t" << execTimes[2] << "secs, \tSpeedUp: " << sup[2] << "\n";
  cout << "MM:" << "\t" << execTimes[3] << "secs, \tSpeedUp: " << sup[3] << "\n";

  double eff[9] = {0.0};
  int np[5] = {1,2,4,8,16};
  for (auto i=4; i<9; ++i)
  {
    eff[i] = (execTimes[4] / execTimes[i]) / static_cast<double>(np[i-4]);
  }
  cout << "PSN(1):" << "\t" << execTimes[4] << "secs, \tSpeedUp: " << sup[4]
       << "\tEfficiency: " << eff[4] << "\n";
  cout << "PSN(2):" << "\t" << execTimes[5] << "secs, \tSpeedUp: " << sup[5]
       << "\tEfficiency: " << eff[5] << "\n";
  cout << "PSN(4):" << "\t" << execTimes[6] << "secs, \tSpeedUp: " << sup[6]
       << "\tEfficiency: " << eff[6] << "\n";
  cout << "PSN(8):" << "\t" << execTimes[7] << "secs, \tSpeedUp: " << sup[7]
       << "\tEfficiency: " << eff[7] << "\n";
  cout << "PSN(16):" << "\t" << execTimes[8] << "secs, \tSpeedUp: " << sup[8]
       << "\tEfficiency: " << eff[8] << "\n";

  return 0;
}
