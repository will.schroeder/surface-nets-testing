// This is a program to run the parallel surface nets algorithm (PSN).
// The input to the program is a volume with discrete labels
// (e.g., segmented label map).
//

// This program runs the algorithm NumSamples times to average execution
// times. After each execution, the algorithm memory is released and the
// algorithm is run again until NumSamples executions occur. The execution
// is wired to occur with 16 threads.
//

#include "vtkActor.h"
#include "vtkCellArray.h"
#include "vtkDataArray.h"
#include "vtkDiscreteFlyingEdges3D.h"
#include "vtkDiscreteMarchingCubes.h"
#include "vtkImageData.h"
#include "vtkLookupTable.h"
#include "vtkMath.h"
#include "vtkNrrdReader.h"
#include "vtkPointData.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkSMPTools.h"
#include "vtkStructuredPointsReader.h"
#include "vtkStructuredPoints.h"
#include "vtkSurfaceNets3D.h"
#include "vtkWindowedSincPolyDataFilter.h"
#include "vtkXMLImageDataReader.h"
#include "vtkTimerLog.h"


// Define test parameters
struct TestParams
{
  std::string                     FileName;
  vtkSmartPointer<vtkImageData>   Image;
  unsigned short*                 Data;
  int                             Dims[3];
  double                          Spacing[3];
  int                             NumLabels;
  int                             NumSmoothingIters;
  double                          ConstraintScale;
  vtkSmartPointer<vtkLookupTable> LUT;
  bool                            Display;
  int                             NumThreads;
  int                             NumSamples;

  // Generate a lookup table. Here we are presuming that label 0 is a
  // background label, and that the labels are contiguous ranging from
  // [1,NumLabels].
  void GenerateLUT()
  {
    vtkNew<vtkMath> math;
    this->LUT = vtkSmartPointer<vtkLookupTable>::New();
    vtkLookupTable* lut = this->LUT.Get();
    lut->SetNumberOfColors(this->NumLabels+1);
    lut->SetTableRange(0, this->NumLabels);
    lut->SetScaleToLinear();
    lut->Build();
    lut->SetTableValue(0, 0, 0, 0, 1);
    math->RandomSeed(5071);
    for ( int i=1; i < this->NumLabels; ++i)
    {
      lut->SetTableValue(i, math->Random(.2, 1),
                            math->Random(.2, 1),
                            math->Random(.2, 1), 1);
    }
  }
};

// Optional display method
void DisplayOutput(vtkPolyData* output, TestParams& testParams)
{
  vtkNew<vtkPolyDataMapper> pdm;
  pdm->SetInputData(output);
  pdm->SetLookupTable(testParams.LUT);
  pdm->SetScalarRange(0, testParams.LUT->GetNumberOfColors());
  pdm->SetColorModeToMapScalars();

  vtkNew<vtkActor> actor;
  actor->SetMapper(pdm);

  vtkNew<vtkRenderer> ren;
  ren->AddActor(actor);

  vtkNew<vtkRenderWindow> renWin;
  renWin->SetSize(500,500);
  renWin->AddRenderer(ren);

  vtkNew<vtkRenderWindowInteractor> iren;
  iren->SetRenderWindow(renWin);

  renWin->Render();
  iren->Start();
}

// Execute parallel surface nets. Return time to average execution results.
double SurfaceNets(TestParams& testParams)
{
  cout << "\nExecuting Parallel Surface Nets\n";
  cout << "\twith " << testParams.NumThreads << " threads\n";
  vtkSMPTools::Initialize(testParams.NumThreads);

  // Time surface nets
  vtkNew<vtkTimerLog> timer;
  double aveTime = 0.0;

  vtkNew<vtkSurfaceNets3D> sn;
  sn->SetInputData(testParams.Image);
  sn->GenerateValues(testParams.NumLabels,1,testParams.NumLabels);
  sn->SetNumberOfIterations(testParams.NumSmoothingIters);
  sn->SetConstraintScale(testParams.ConstraintScale);

  for (int testNum=0; testNum < testParams.NumSamples; ++testNum)
  {
    sn->Modified();
    timer->StartTimer();
    sn->Update();
    timer->StopTimer();
    cout << "\n";
    aveTime += timer->GetElapsedTime();
  }
  aveTime /= static_cast<double>(testParams.NumSamples);

  cout << "Time for Parallel Surface Nets (averaged): " << aveTime << "\n";
  cout << "# triangles: " << sn->GetOutput()->GetNumberOfCells() << "\n";

  if (testParams.Display)
    DisplayOutput(sn->GetOutput(),testParams);

  return aveTime;
}

// Reads data of a few select types. Keys off of input file extension.
vtkSmartPointer<vtkImageData> ReadData(TestParams& testParams)
{
  vtkSmartPointer<vtkImageData> output;

  std::string extension = testParams.FileName.substr(testParams.FileName.find_last_of(".") + 1);

  if (extension == "vti")
  {
    vtkNew<vtkXMLImageDataReader> reader;
    reader->SetFileName(testParams.FileName.c_str());
    reader->Update();
    output = reader->GetOutput();
  }
  else if (extension == "vtk")
  {
    vtkNew<vtkStructuredPointsReader> reader;
    reader->SetFileName(testParams.FileName.c_str());
    reader->Update();
    output = reader->GetOutput();
  }
  else if (extension == "nrrd")
  {
    vtkNew<vtkNrrdReader> reader;
    reader->SetFileName(testParams.FileName.c_str());
    reader->Update();
    output = reader->GetOutput();
  }
  else
  {
    cout << "Unable to open file: " << testParams.FileName << "\n";
    exit(1);
  }

  return output;
}

// Test driver main(). There are up to four command line arguments. The first
// required argument is the name of the labeled volume / annotation file. The
// second (required) argument is the number of labels. The third (optional)
// argument is the number of samples/repeated executions to average
// performance (by default NumSamples==1). The fourth (optional) argument is
// a flag indicating whether to display output afer each algorithm execution.
int main(int argc, char *argv[])
{
  // Command line args. Quick and dirty parsing.
  if ( argc < 3 )
  {
    cout << "Must provide annotation file name, required numSamples, optional numExecs, optional display flag\n";
    cout << argv[0] << " fileName numLabels numSamples display\n";
    exit(1);
  }

  // Specify parameters
  TestParams testParams;
  testParams.FileName = argv[1];
  testParams.NumLabels = atoi(argv[2]);
  testParams.NumSamples = ( argc < 4 ? 1 : atoi(argv[3]));
  testParams.Display = (argc < 5 ? false : true);
  cout << "Processing: " << testParams.FileName << " with "
       << testParams.NumLabels << " labels, "
       << testParams.NumSamples << " samples and display "
       << (testParams.Display ? "On" : "Off") << "\n";

  // Load in test file
  vtkSmartPointer<vtkImageData> output = ReadData(testParams);
  testParams.Image = output;

  // Get the data
  testParams.Data = (unsigned short*) testParams.Image->GetScalarPointer();
  testParams.Image->GetDimensions(testParams.Dims);
  testParams.Image->GetSpacing(testParams.Spacing);
  cout << "Read volume from: " << testParams.FileName << " with \n\tDimensions: ("
       << testParams.Dims[0] << "," << testParams.Dims[1] << "," << testParams.Dims[2] << ")\n";
  cout << "\tSpacing: ("
       << testParams.Spacing[0] << "," << testParams.Spacing[1] << "," << testParams.Spacing[2] << ")\n";

  // Set remaining control parameters
  testParams.NumSmoothingIters = 25;
  testParams.ConstraintScale = 2.0;
  cout << "\tNumLabels, NumSmoothingIters: (" << testParams.NumLabels << ", " << testParams.NumSmoothingIters << ")\n";
  testParams.GenerateLUT(); // Lookup table for displaying data

  // Execute different algorithms. The vtkSMPTools::Initialize(numThreads)
  // is used to control threading. In some cases we want to run sequential
  // by setting numThreads=1. This affects not only the surface extraction,
  // but also the smoothing operation.

  // Keep track of averaged execution times.
  double execTime;

  // Parallel surface nets. Both surface extraction and constrained smoothing
  // are affected by the thread count.
  testParams.NumThreads = 16;
  execTime = SurfaceNets(testParams);

  // Output test results
  cout << "PSN(16):" << "\t" << execTime << "\n";

  return 0;
}
